import { Component, OnDestroy, OnInit } from '@angular/core';

import { first } from 'rxjs/operators';

import { ConfigService } from './service/config.service';
import { DataService } from './service/data.service';
import { COMMON_LANG, HttpService, IInitializationData, YAML } from './service/http.service';
import { LoggerService, MODE } from './service/logger.service';

import { URL_PARAMETERS } from './enums/url-parameters';
import { Configuration } from './vo/configuration';

/**
 * Model of language files to load.
 */
interface ILangItemToLoad {
	locale: string;
	files: Array<string>;
}

/**
 * Main application component.
 */
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of AppComponent.
	 *
	 * @param {LoggerService} logger
	 * @param {HttpService} httpService
	 * @param {DataService} dataService
	 * @param {ConfigService} configService
	 */
	constructor(
		private readonly logger: LoggerService,
		private readonly httpService: HttpService,
		private readonly dataService: DataService,
		private readonly configService: ConfigService
	) {}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Init application.
	 * This is main entry point.
	 *
	 * @returns {void}
	 */
	private initApp(): void {
		this.logger.log(MODE.log, 'Init config. Current user locale:', this.configService.environment.locale);

		// load configuration and actions
		this.httpService.loadConfigurationAndActions()
			.pipe(first())
			.subscribe((result: IInitializationData) => {
				this.logger.log(MODE.log, 'Configuration and script OK:', result);

				const configuration: Configuration = result.configuration;
				configuration.urlParameters = this.configService.parseURLParameters();
				configuration.langPackHashMap = new Map<string, string>();

				// check URL "id" parameter
				if (configuration.urlParameters.has(URL_PARAMETERS[URL_PARAMETERS.id])) {
					const arr: Array<string> = configuration.urlParameters.get(URL_PARAMETERS[URL_PARAMETERS.id]);
					this.logger.log(MODE.log, 'Detected list of actions ID to show:', arr);
					this.dataService.setActionsToShow(arr);
				}

				// check URL "filter" parameter and set initial filters
				if (configuration.urlParameters.has(URL_PARAMETERS[URL_PARAMETERS.filter])) {
					const arrF: Array<string> = configuration.urlParameters.get(URL_PARAMETERS[URL_PARAMETERS.filter]);
					const mapF: Map<string, string> = new Map();
					arrF.forEach(f => {
						const arrFI: Array<string> = f.split(',');
						mapF.set(arrFI[0], arrFI[1]);
					});
					this.dataService.setInitialFilters(mapF);
				}

				// make possible locales list
				const longLangLocale: string = this.configService.environment.locale;
				const shortLangLocale: string = longLangLocale.split('-')[0];
				const localeAliasList: Array<string> = [
					longLangLocale,
					shortLangLocale,
					configuration.actionLanguage
				];

				// check URL for language link like a "http://domain.com/test/just/XX/for/example"
				const href = document.location.href;
				if (href) {
					const matches = href.match(/\/[a-z]{2}\//);
					const urlLang = matches && matches.length ? matches[0].replace(/\//g, '') : undefined;
					if (urlLang && urlLang in configuration.languages) {
						localeAliasList.unshift(urlLang);
					}
				}

				// check URL "lang" parameter like a "http://domain.com/test?lang=XX"
				if (configuration.urlParameters.has(URL_PARAMETERS[URL_PARAMETERS.lang])) {
					const ul: string = configuration.urlParameters.get(URL_PARAMETERS[URL_PARAMETERS.lang])[0];
					localeAliasList.unshift(ul);
				}

				// create array of possible lang items to load
				const loadList: Array<ILangItemToLoad> = localeAliasList
					.map(m => {
						let files: Array<string>;
						if (configuration.languages[m]) {
							files = Array.isArray(configuration.languages[m])
								? configuration.languages[m]
								: [configuration.languages[m]];
						}

						return {
							locale: m,
							files
						};
					})
					.filter(p => p.files);

				// if array isn't empty we can request prepared items
				if (loadList.length > 0) {
					const firstItemToLoad = loadList[0];
					const commonLang: string = COMMON_LANG + firstItemToLoad.locale + YAML;

					this.httpService.loadLanguageFiles([...firstItemToLoad.files, commonLang])
						.subscribe(langPackHashMap => {
							configuration.langPackHashMap = langPackHashMap;

							// update window title if resource is present
							if (langPackHashMap.has('app-title')) {
								document.title = langPackHashMap.get('app-title');
							}

							// init app and start
							configuration.actionLanguage = firstItemToLoad.locale;
							this.configService.setConfiguration(configuration);
							this.dataService.setActions(result.actions);
						}, errorLang => {
							this.logger.log(MODE.error, 'Check configuration of languages files!', errorLang);
						});
				}
			}, errorInit => {
				this.logger.log(MODE.error, 'Check configuration or actions file!', errorInit);
			});
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
		this.initApp();
	}

	ngOnDestroy(): void {
	}
}
