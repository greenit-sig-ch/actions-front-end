/**
 * Simple model containing some properties: ID, Value and Color.
 */
export interface IdValueColor {
	id: string;
	maxValue: number;
	color: string;
}
