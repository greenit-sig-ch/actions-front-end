/**
 * Action model.
 */
export interface Action {
	/**
	 * Action ID.
	 */
	id: string;

	/**
	 * Hashmap containing filter list for curent action.
	 */
	filter: Map<string, Array<string>>;

	/**
	 * Hashmap containing impact list for curent action.
	 */
	impact: Map<string, number>;

	/**
	 * Hashmap containing effort list for curent action.
	 */
	effort: Map<string, number>;
}
