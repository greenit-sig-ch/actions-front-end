/**
 * User environment model.
 */
export interface Environment {
	/**
	 * Indicates that this system is Windows.
	 */
	isWindows: boolean;

	/**
	 * Indicates that this system is iOS.
	 */
	isMac: boolean;

	/**
	 * Indicates that this system is Linux.
	 */
	isLinux: boolean;

	/**
	 * It's the IE browser.
	 */
	isIE: boolean;

	/**
	 * It's the Chrome browser.
	 */
	isChrome: boolean;

	/**
	 * It's the Firefox browser.
	 */
	isFirefox: boolean;

	/**
	 * It's the Safari browser.
	 */
	isSafari: boolean;

	/**
	 * Possible browser version.
	 */
	version: number;

	/**
	 * Current users locale.
	 * String like a "ua-UA".
	 */
	locale: string;
}
