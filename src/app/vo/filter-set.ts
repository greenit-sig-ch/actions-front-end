import { IdValueColor } from '../interfaces/id-value-color';

export interface FilterType {
	[key: string]: string;
}

export interface EffortsType {
	[key: string]: [number, number];
}

/**
 * Filter set model.
 */
export class FilterSet {

	// -----------------------------
	//  Constants
	// -----------------------------

	static IMPACT = 'impact';

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Common filters and impact filter values.
	 * Component like a dropdown.
	 */
	filters: FilterType;

	/**
	 * Efforts filter values.
	 * Component like a slider.
	 */
	efforts: EffortsType;
	
	// -----------------------------
	//  Private properties
	// -----------------------------
		
	private effortsHashmap: Map<string, IdValueColor>;
	
	// -----------------------------
	//  Public functions
	// -----------------------------
	
	/**
	 * Creates an instance of FilterSet.
	 *
	 * @param efforts
	 */
	constructor(efforts: Array<IdValueColor>) {
		this.filters = {impact: ''};
		this.efforts = {};

		this.effortsHashmap = new Map<string, IdValueColor>();
		efforts.forEach(effort => {
			this.efforts[effort.id] = [ 0, effort.maxValue ];
			this.effortsHashmap.set(effort.id, effort);
		});
	}

	/**
	 * Reset filter set.
	 *
	 * @return {void}
	 */
	reset(): void {
		Object.keys(this.filters)
			.forEach(fn => this.filters[fn] = '');

		const efforts: EffortsType = {};
		this.effortsHashmap.forEach((v: IdValueColor, k: string) => efforts[k] = [0, v.maxValue]);
		this.efforts = efforts;
	}

}

