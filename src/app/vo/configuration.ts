import { MODE } from '../service/logger.service';

/**
 * Application configuration model.
 */
export class Configuration {
	/**
	 * Configuration version.
	 */
	version = 1;

	/**
	 * Path to the script file like a "content/actions.yaml".
	 */
	actionFile: string;

	/**
	 * Current language used for UI.
	 */
	actionLanguage: string;

	/**
	 * Undefined strings control.
	 * If <code>true</code> will show undefined string like a <code>%key%</code> otherwise will show empty string.
	 */
	showUndefinedStrings: boolean;

	/**
	 * Language file object.
	 */
	languages: any;

	/**
	 * Logging level.
	 */
	logLevel: MODE;

	/**
	 * The URL parameters specified during application startup.
	 */
	urlParameters: Map<string, Array<string>>;

	// -----------------------------
	//  Calculated properties
	// -----------------------------

	langPackHashMap: Map<string, string>;
}
