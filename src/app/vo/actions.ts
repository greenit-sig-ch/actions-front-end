import { Action } from '../interfaces/action';
import { IdValueColor } from '../interfaces/id-value-color';

/**
 * Actions model.
 */
export class Actions {
	/**
	 * The action list's version number.
	 */
	version: number;

	/**
	 * The actions (human readable) identification string.
	 */
	id: string;

	/**
	 * Hashmap containing common filter list.
	 * Filter type definitions:
	 * In this example, we have three filter types: audience, lifecycle and category plus their
	 * corresponding values. The actual titles that must be displayed by the app are stored in
	 * the language file; the key is the filter / value name plus "-title", e.g. audience-title
	 * or planning-title.
	 * The "All" selection that is displayed in the drop-down menus on the webpage must be added
	 * by the action list app.
	 */
	filter: Map<string, Array<string>>;

	/**
	 * Array of all impact items.
	 * Impact type definitions.
	 * Similar to the filter type definition above, the action liste developer can define any
	 * number of impact types, here: ecological, economic and energy.
	 * On the website, the user can either choose one of the impact types or "All" from a drop-down menu.
	 */
	impact: Array<IdValueColor>;

	/**
	 * Array of all effort items.
	 * Effort type definitions.
	 * Similar to the filter type definition above, the action liste developer can define any
	 * number of effort types, here: cost and time.
	 * On the website, the user can define the range of values that actions must have with
	 * range sliders (one for each effort type).
	 */
	effort: Array<IdValueColor>;

	/**
	 * List of the action models.
	 * Action definitions.
	 * Compared to the original action definition, this one has been simplified significantly.
	 * All textual description that has been stored in separate fields is put into the action's
	 * description by the action list developer.
	 * For each action, append the following strings to the action's id to get
	 * the <b>title</b>, <b>summary</b> and <b>description</b> from the language file:
	 * <ul>
	 *   <li>"-title"</li>
	 *   <li>"-summary"</li>
	 *   <li>"-description"</li>
	 * <ul>
	 */
	actions: Array<Action>;

	// -----------------------------
	//  Calculated properties
	// -----------------------------

	/**
	 * Array of common filters type.
	 *
	 * @type {Array<string>}
	 */
	filterTypes: Array<string>;

	effortMap: Map<string, IdValueColor>;
	impactMap: Map<string, IdValueColor>;
}
