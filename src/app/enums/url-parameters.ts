/**
 * Known URL parameters.
 * - {@link lang} - current language used (example: http://site.com?lang=de).
 * - {@link id} - filter actions by ID (example: http://site.com?id=test_action_id).
 * - {@link filter} - filter actions by category/value pair (example: http://site.com?filter=category,value).
 */
export enum URL_PARAMETERS {
	lang,
	id,
	filter
}
