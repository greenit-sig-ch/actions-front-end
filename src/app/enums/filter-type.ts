/**
 * Set of filter types.
 */
export enum FilterType {
	'audience' = 0,
	'lifecycle' = 1,
	'category' = 2,
	'impact' = 3,
	'effort' = 4
}
