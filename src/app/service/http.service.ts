import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { forkJoin, map, Observable, Subject, switchMap } from 'rxjs';

import { Md5 } from 'ts-md5/dist/md5';

import { Actions } from '../vo/actions';
import { Configuration } from '../vo/configuration';
import { LoggerService, MODE } from './logger.service';

import * as jsyaml from 'js-yaml';

/**
 * Initialization data model.
 */
export interface IInitializationData {
	actionsHash: string;
	configuration: Configuration;
	actions: Actions;
}

// -----------------------------
//  Constants
// -----------------------------

export const CONFIG_FILE = 'content/config.yaml';
export const COMMON_LANG = 'assets/action-list-front-end-';
export const YAML = '.yaml';

@Injectable({
	providedIn: 'root'
})
export class HttpService {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Constructor of service.
	 *
	 * @param http
	 * @param logger
	 */
	constructor(
		private readonly http: HttpClient,
		private readonly logger: LoggerService,
	) {}

	/**
	 * Load configuration and action files.
	 *
	 * @returns {Observable<IInitializationData>}
	 */
	loadConfigurationAndActions(): Observable<IInitializationData> {
		this.logger.log(MODE.log, 'Loading configuration and script files...');
		
		return this.http.get(CONFIG_FILE, { responseType: 'text' })
			.pipe(
				map((responseConfig: string) => jsyaml.load(responseConfig) as Configuration),
				switchMap((configuration: Configuration) =>
					this.http.get(configuration.actionFile, { responseType: 'text' })
						.pipe(
							map((actionResponse: string) => {
								this.logger.setCurrentMode(configuration.logLevel);
								
								const actions: Actions = jsyaml.load(actionResponse) as Actions;
								const actionsHash: string = Md5.hashStr(actionResponse) as string;
								
								return {actionsHash, configuration, actions};
							})
						))
			);
	}

	/**
	 * Load languages files by filename list.
	 *
	 * @param {Array<string>} langFiles List of files to load.
	 */
	loadLanguageFiles(langFiles: Array<string>): Observable<Map<string, string>> {
		this.logger.log(MODE.log, 'Loading files:', langFiles);

		const result = new Subject<Map<string, string>>();
		const map = new Map<string, string>();
		const observables = langFiles.map(observable => {
			return this.http.get(observable, {responseType: 'text'});
		});
		forkJoin(observables)
			.subscribe(responses => {
				if (responses) {
					responses.forEach(res => {
						const obj = jsyaml.load(res);
						Object.keys(obj)
							.forEach(key => map.set(key, obj[key]));
					});
				}
			}, error => {
				result.error(error);
			}, () => {
				result.next(map);
			});

		return result;
	}
}
