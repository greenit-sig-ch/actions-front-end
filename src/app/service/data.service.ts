import { Injectable } from '@angular/core';

import { LoggerService, MODE } from './logger.service';

import { BehaviorSubject, Observable } from 'rxjs';
import { Action } from '../interfaces/action';
import { IdValueColor } from '../interfaces/id-value-color';
import { LocalizationPipe } from '../pipe/localization.pipe';
import { Actions } from '../vo/actions';
import { FilterSet } from '../vo/filter-set';

/**
 * Parse filter property to hashmap Map<string, T>.
 *
 * @param {any} sourceObject
 * @returns {Map<string, any>}
 */
const parseObjectToHashmap = <T>(sourceObject: any): Map<string, T> => {
	const map: Map<string, T> = new Map<string, T>();
	if (Array.isArray(sourceObject)) {
		sourceObject
			.forEach(ao => {
				Object.keys(ao)
					.forEach(key => {
						map.set(key, ao[key]);
					});
			});
	}

	return map;
};

/**
 * Application data service.
 */
@Injectable({
	providedIn: 'root'
})
export class DataService {

	// -----------------------------
	//  Public properties
	// -----------------------------
	
	/**
	 * Emits a value with new actions list on {@link setActions} method.
	 */
	readonly actionSubject$: Observable<Actions>;
	
	readonly filteredActions$ = new BehaviorSubject<Array<Action>>([]);

	// -----------------------------
	//  Private properties
	// -----------------------------

	private readonly actionSource = new BehaviorSubject<Actions>(undefined);
	
	private _actions: Actions;
	private _filterSet: FilterSet;
	private _initialFilters: Map<string, string> = new Map<string, string>();
	// private _filteredActions: Array<Action> = [];
	private _actionsToShow: Array<string>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of DataService.
	 *
	 * @param {LoggerService} logger
	 * @param {LocalizationPipe} lang
	 */
	constructor(
		private readonly logger: LoggerService,
		private readonly lang: LocalizationPipe
	) {
		this.actionSubject$ = this.actionSource.asObservable();
	}

	/**
	 * Set actions.
	 *
	 * @param {Actions} actions Instance of Actions model.
	 */
	setActions(actions: Actions): void {
		this._actions = actions;
		this.parseActions();
		this.actionSource.next(this._actions);
	}

	/**
	 * Return application configuration.
	 *
	 * @returns Instance of Configuration model.
	 */
	getActions(): Actions {
		return this._actions;
	}

	// /**
	//  * Get filtered actions.
	//  *
	//  * @returns {Array<Action>}
	//  */
	// getFilteredActions(): Array<Action> {
	// 	return this._filteredActions;
	// }

	getFilterSet(): FilterSet {
		return this._filterSet;
	}

	/**
	 * Return list of action to show which was taken from URL.
	 *
	 * @return {Array<string>}
	 */
	getActionsToShow(): Array<string> {
		return this._actionsToShow;
	}

	/**
	 * Set list of action to show which was taken from URL.
	 *
	 * @param {Array<string>} arr
	 * @return {void}
	 */
	setActionsToShow(arr: Array<string>): void {
		this._actionsToShow = arr;
	}

	/**
	 * Set initial filters.
	 *
	 * @param {Map<string, string>} value
	 */
	setInitialFilters(value: Map<string, string>): void {
		this._initialFilters = value;
	}

	/**
	 * Perform filter the actions by filter set and current page.
	 *
	 * @returns {void}
	 */
	performFilterActions(): void {
		const arr: Array<Action> = [];
		this.getActions().actions
			.forEach(a => {
				// apply action filter
				let showAction = true;

				// parse common filters and impact
				Object.keys(this._filterSet.filters)
					.forEach(fn => {
						if (showAction) {
							const fv = this._filterSet.filters[fn];

							// skip "" value
							if (fv.length) {
								if (fn === FilterSet.IMPACT) {
									// check impact
									if (a.impact.has(fv)) {
										const iv = a.impact.get(fv);
										const ivc = this.getActions().impact
											.find(aiv => aiv.id === fv);
										if (ivc) {
											showAction = showAction && (iv >= 0 && iv <= ivc.maxValue);
										} else {
											this.logger.log(MODE.warn, 'Unknown impact:', fv);
										}
									} else {
										showAction = false;
									}
								} else {
									// check common filters
									showAction = showAction && a.filter.get(fn)
										.indexOf(fv) !== -1;
								}
							}
						}
					});

				// parse effort filter
				if (showAction) {
					a.effort.forEach((ev, ek) => {
						if (this._filterSet.efforts[ek]) {
							const fev = this._filterSet.efforts[ek];
							showAction = showAction && ev >= fev[0] && ev <= fev[1];
						}
					});
				}

				// add action to filtered array
				if (showAction) {
					arr.push(a);
				}
			});

		// this._filteredActions = arr;
		// this.logger.log(MODE.log, 'Filtered actions:', this._filteredActions);
		this.filteredActions$.next(arr);
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Parse actions configuration.
	 *
	 * @returns {void}
	 */
	private parseActions(): void {

		// sort actions
		this.getActions().actions
			.sort((a, b) => {
				const ai: string = this.lang.transform('title', a.id);
				const bi: string = this.lang.transform('title', b.id);

				if (ai > bi) {
					return 1;
				} else if (ai < bi) {
					return -1;
				}

				return 0;
			});

		// filter actions by URL properties
		if (this._actionsToShow) {
			const arr: Array<Action> = [];
			this.getActions().actions
				.forEach(a => {
					if (this._actionsToShow && this._actionsToShow.indexOf(a.id) >= 0) {
						arr.push(a);
					}
				});
			this.getActions().actions = arr;
			this.logger.log(MODE.log, 'Action was filtered by URL property:', this.getActions().actions);
		}

		this._filterSet = new FilterSet(this.getActions().effort);

		// make common hashmap of all filters
		this.getActions().filter = parseObjectToHashmap<Array<string>>(this.getActions().filter);

		// make efforts hashmap
		const em: Map<string, IdValueColor> = new Map<string, IdValueColor>();
		this.getActions().effort
			.forEach(ev => {
				ev.color = `#${ev.color}`;
				em.set(ev.id, ev);
			});
		this.getActions().effortMap = em;

		// make impacts hashmap
		const im: Map<string, IdValueColor> = new Map<string, IdValueColor>();
		this.getActions().impact
			.forEach(iv => {
				iv.color = `#${iv.color}`;
				im.set(iv.id, iv);
			});
		this.getActions().impactMap = im;

		// make hashmap for all actions
		this.getActions().actions
			.forEach(action => {
				action.filter = parseObjectToHashmap<Array<string>>(action.filter);
				action.impact = parseObjectToHashmap<number>(action.impact);
				action.effort = parseObjectToHashmap<number>(action.effort);
			});

		// make common filter array
		this.getActions().filterTypes = Array.from(this.getActions().filter
			.keys());
		this.getActions().filterTypes
			.forEach(fn => this._filterSet.filters[fn] = '');

		this._initialFilters.forEach((fav, fak) => this._filterSet.filters[fak] = fav);

		this.performFilterActions();
	}
}
