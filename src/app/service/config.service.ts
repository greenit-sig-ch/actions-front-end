import { Injectable } from '@angular/core';
import { Environment } from '../interfaces/environment';
import { Configuration } from '../vo/configuration';
import { LoggerService, MODE } from './logger.service';

/**
 * Config service.
 */
@Injectable({
	providedIn: 'root'
})
export class ConfigService {

	// -----------------------------
	//  Private properties
	// -----------------------------

	private _configuration: Configuration;
	private readonly _environment: Environment = {
		isChrome: false,
		isFirefox: false,
		isIE: false,
		isLinux: false,
		isMac: false,
		isSafari: false,
		isWindows: false,
		locale: undefined,
		version: undefined
	};

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	get environment(): Environment {
		return this._environment;
	}

	// -----

	/**
	 * Set application configuration.
	 *
	 * @param {Configuration} configuration Instance of Configuration model.
	 * @returns {void}
	 */
	setConfiguration(configuration: Configuration): void {
		this.logger.log(MODE.log, 'Application configuration:', configuration);
		this._configuration = configuration;
	}
	/**
	 * Return application configuration.
	 *
	 * @returns {Configuration} Instance of Configuration model.
	 */
	getConfiguration(): Configuration {
		return this._configuration;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of ConfigService.
	 *
	 * @param {LoggerService} logger
	 */
	constructor(
		private readonly logger: LoggerService
	) {}

	/**
	 * Init application.
	 *
	 * @returns {void}
	 */
	initConfiguration(): void {
		this.logger.log(MODE.log, 'Init application on load...');
		this.detectEnvironment();
	}

	/**
	 * Parse URL parameters.
	 *
	 * @returns {Map<string, string>}
	 */
	parseURLParameters(): Map<string, Array<string>> {
		const result = new Map<string, Array<string>>();

		if (window.location.search && window.location.search.length > 0) {
			const urlStr: string = decodeURIComponent(window.location.search.substring(1));
			urlStr.split('&')
				.forEach(p => {
					const arr: Array<string> = p.split('=');
					if (arr.length === 2) {
						if (result.has(arr[0])) {
							const i: Array<string> = result.get(arr[0]);
							i.push(arr[1]);
						} else {
							result.set(arr[0], new Array(arr[1]));
						}
					}
				});
		}

		return result;
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Detect system environment.
	 *
	 * @returns {void}
	 */
	private detectEnvironment(): void {
		const navigator: Navigator = window.navigator;
		const platform: string = navigator.platform;
		const appVersion: string = navigator.appVersion;
		const userAgent: string = navigator.userAgent;

		this._environment.isWindows = /^win32/i.test(platform) || /win/i.test(appVersion);
		this._environment.isMac = /^mac/i.test(platform) || /mac/i.test(appVersion);
		this._environment.isLinux = /^linux/i.test(platform) || /linux/i.test(appVersion);

		// MSIE check taken from Angular core
		const ie: number = Number(document['documentMode']);
		this._environment.isChrome = /chrome/i.test(userAgent);
		this._environment.isFirefox = /firefox/i.test(userAgent);
		this._environment.isSafari = /safari/i.test(userAgent);
		this._environment.isSafari = !this._environment.isChrome && this._environment.isSafari;

		let arr: RegExpMatchArray = userAgent.match(/version\/(\d+(\.\d+)?)/i);
		let version: string = arr && arr.length ? arr[1] : undefined;
		if (!isNaN(ie)) {
			this._environment.isIE = true;
			arr = userAgent.match(/(?:msie |rv:)(\d+(\.\d+)?)/i);
			version = arr && arr.length ? arr[1] : undefined;
		}
		this._environment.version = parseFloat(version);

		// detect locale
		const browserLanguagePropertyKeys = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'];
		if (Array.isArray(navigator.languages)) {
			this._environment.locale = navigator.languages.find(l => l && String(l).length > 0);
		} else {
			const lv = browserLanguagePropertyKeys.find(lk =>	navigator[lk] && String(navigator[lk]).length > 0);
			this._environment.locale = navigator[lv];
		}

		this.logger.log(MODE.log, 'Current environment configuration:', this._environment);
	}
}
