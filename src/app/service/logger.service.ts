import { Injectable } from '@angular/core';

/**
 * Logger mode enum.
 * Allowed modes:
 * - {@link log}
 * - {@link info}
 * - {@link warn}
 * - {@link error}
 * - {@link off}
 */
export enum MODE {
	log = 0,
	info = 1,
	warn = 2,
	error = 3,
	off = 5
}

/**
 * Logger service.
 */
@Injectable({
	providedIn: 'root'
})
export class LoggerService {

	// -----------------------------
	//  Private properties
	// -----------------------------

	private _currentMode: MODE = MODE.log;

	// -----------------------------
	//  Public functions
	// -----------------------------

	getCurrentMode(): MODE {
		return this._currentMode;
	}

	setCurrentMode(mode: MODE): void {
		console.log(`Logger mode will change from [${MODE[this._currentMode]}] to [${MODE[mode]}]`);
		this._currentMode = mode;
	}

	log(mode: MODE, ...value: Array<any>): void {
		if (this._currentMode !== MODE.off && mode >= this._currentMode) {
			window.console[MODE[mode]](`${new Date().toISOString()} [${MODE[mode]}]:`, value);
		}
	}
}
