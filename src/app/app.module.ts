import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

import { AppRoutingModule } from './app-routing.module';

import { NgxMdModule } from 'ngx-md';

import { ConfigService } from './service/config.service';
import { DataService } from './service/data.service';
import { HttpService } from './service/http.service';
import { LoggerService } from './service/logger.service';

import { LocalizationPipe } from './pipe/localization.pipe';

import { AppComponent } from './app.component';
import { ActionComponent } from './view/action/action.component';
import { ActionsComponent } from './view/actions/actions.component';
import { EffortComponent } from './view/effort/effort.component';
import { FilterComponent } from './view/filter/filter.component';
import { HorizontalBarComponent } from './view/horizontal-bar/horizontal-bar.component';

/**
 * Main init factory function.
 *
 * @param {ConfigService} configService
 * @returns {Function}
 */
export const startupServiceFactory = (configService: ConfigService): Function => () => {
	configService.initConfiguration();
};

/**
 * Main application module.
 *
 * @export
 * @class AppModule
 */
@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpClientModule,
		AppRoutingModule,

		ScrollingModule,
		NgxSliderModule,
		PopoverModule.forRoot(),
		ModalModule.forRoot(),
		NgxMdModule.forRoot(),
	],
	declarations: [
		AppComponent,
		LocalizationPipe,
		ActionsComponent,
		FilterComponent,
		ActionComponent,
		EffortComponent,
		HorizontalBarComponent,
	],
	providers: [
		HttpService,
		DataService,
		LoggerService,
		LocalizationPipe,
		ConfigService,
		{
			provide: APP_INITIALIZER,
			useFactory: startupServiceFactory,
			deps: [ConfigService],
			multi: true
		}
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {}
