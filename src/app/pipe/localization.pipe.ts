import { Pipe, PipeTransform } from '@angular/core';
import { ConfigService } from '../service/config.service';

/**
 * Localization pipe class.
 * Function <b>transform</b> will try to localize string resources.
 */
@Pipe({
	name: 'lang',
	pure: false
})
export class LocalizationPipe implements PipeTransform {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of LocalizationPipe.
	 *
	 * @param {ConfigService} configService
	 */
	constructor(
		private readonly configService: ConfigService
	) {}

	/**
	 * String transformer function.
	 * Return string resource by key <b>id-strkey</b> if or just by <b>strkey</b> if id is undefined.
	 *
	 * @param {string} strKey
	 * @param {string} id
	 * @param {Array<string>} prop Array of values for localization properties.
	 */
	transform(strKey: string, id?: string, prop?: Array<string>): string {
		const key: string = (id !== undefined && id !== null ? `${id}-${strKey}` : strKey);
		if (strKey
			&& this.configService.getConfiguration()
			&& this.configService.getConfiguration().langPackHashMap
			&& this.configService.getConfiguration().langPackHashMap
				.has(key)
		) {
			let result: string = this.configService.getConfiguration().langPackHashMap
				.get(key);
			if (prop !== undefined) {
				const rma: RegExpMatchArray = result.match(/\{\{[0-9a-z\sA-Z]*\}\}/gi);
				rma.forEach((v, idx) => result = result.replace(v, prop[idx]));
			}

			return result;
		}

		return this.configService.getConfiguration() && this.configService.getConfiguration().showUndefinedStrings ? `%${key}%` : '';
	}

	/**
	 * Checks the availability of the resource.
	 *
	 * @param {string} strKey
	 * @return {boolean} Return <b>true</b> if resource exists or <b>showUndefinedStrings</b> is set to <b>true</b>.
	 */
	checkResourceId(strKey: string): boolean {
		return this.configService.getConfiguration().langPackHashMap
				.has(strKey)
			|| this.configService.getConfiguration().showUndefinedStrings;
	}
}
