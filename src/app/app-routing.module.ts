import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ActionsComponent } from './view/actions/actions.component';

const routes: Routes = [
	// {path: '', redirectTo: 'actions', pathMatch: 'full'},
	// {path: 'actions', component: ActionsComponent, children: []},
	// {path: 'action/:id', component: ActionsComponent},
	// {path: '', redirectTo: '/action', pathMatch: 'full'},
	// {path: 'action', component: ActionsComponent},
	// {path: 'action/:id', component: ActionsComponent}
	{path: '**', component: ActionsComponent, children: []}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
	exports: [RouterModule]
})
export class AppRoutingModule {}
