import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ConfigService } from '../../service/config.service';
import { DataService } from '../../service/data.service';
import { LoggerService, MODE } from '../../service/logger.service';

@Component({
	selector: 'app-actions',
	templateUrl: './actions.component.html',
	styleUrls: ['./actions.component.less'],
	animations: [
		trigger('slideInOut', [
			state('in', style({
				transform: 'translate3d(0, 0, 0)'
			})),
			state('out', style({
				transform: 'translate3d(-100%, 0, 0)'
			})),
			transition('in => out', animate('400ms ease-in-out')),
			transition('out => in', animate('400ms ease-in-out'))
		])
	]
})
export class ActionsComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------

	@ViewChild('lgModal', { static: true })
	lgModal: any;

	menuState = 'out';

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	get showAssessmentLink(): boolean {
		return this.dataService.getActionsToShow() && this.dataService.getActionsToShow().length > 0;
	}

	get logoFileName(): string {
		// avoid 404 errors when the browser tries to get logo_undefined.png before the configuration is read.
		if (typeof this.configService.getConfiguration().actionLanguage !== 'undefined') {
			return `content/img/logo_${this.configService.getConfiguration().actionLanguage}.png`;
		}

		return '';
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		readonly dataService: DataService,
		private readonly configService: ConfigService,
		private readonly logger: LoggerService
	) {}

	/**
	 * Show/hide filter menu for small screens.
	 *
	 * @returns {void}
	 */
	toggleMenu(): void {
		this.menuState = this.menuState === 'out' ? 'in' : 'out';
	}

	focusOpener(): void {
		window.close();
	}

	/**
	 * On help button click handler.
	 *
	 * @param {boolean} isSmallFilter
	 * @returns {void}
	 */
	onHelpClicked(isSmallFilter: boolean): void {
		this.logger.log(MODE.log, this, 'onHelpClicked');
		if (isSmallFilter) {
			this.menuState = 'out';
		}
		this.lgModal.show();
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}

}
