import { Component, Input, OnInit } from '@angular/core';
import { FilterType } from '../../enums/filter-type';
import { Action } from '../../interfaces/action';
import { IdValueColor } from '../../interfaces/id-value-color';
import { LocalizationPipe } from '../../pipe/localization.pipe';
import { DataService } from '../../service/data.service';

@Component({
	selector: 'app-action',
	templateUrl: './action.component.html',
	styleUrls: ['./action.component.less']
})
export class ActionComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Current action model.
	 */
	@Input()
	currentAction: Action;

	// -----------------------------
	//  Public properties
	// -----------------------------

	readonly FilterType = FilterType;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Return impact store.
	 *
	 * @returns {Map<string, IdValueColor>}
	 */
	get impactStore(): Map<string, IdValueColor> {
		return this.dataService.getActions().impactMap;
	}

	/**
	 * Return effort store.
	 *
	 * @returns {Map<string, IdValueColor>}
	 */
	get effortStore(): Map<string, IdValueColor> {
		return this.dataService.getActions().effortMap;
	}

	/**
	 * Return <code>true</code> if action is collapsed.
	 *
	 * @returns {boolean}
	 */
	get isCollapsed(): boolean {
		return this._isCollapsed;
	}

	/**
	 * Return <code>true</code> if efforts map is empty.
	 *
	 * @returns {boolean}
	 */
	get isEmptyEfforts(): boolean {
		return !this.currentAction.effort || !this.currentAction.effort.size;
	}

	/**
	 * Return <code>true</code> if impacts map is empty.
	 *
	 * @returns {boolean}
	 */
	get isEmptyImpacts(): boolean {
		return !this.currentAction.impact || !this.currentAction.impact.size;
	}

	/**
	 * Return array of impacts keys.
	 *
	 * @returns {Array<string>}
	 */
	get impacts(): Array<string> {
		return Array.from(this.currentAction.impact.keys());
	}

	/**
	 * Return array of efforts string.
	 *
	 * @returns {Array<string>}
	 */
	get efforts(): Array<string> {
		return Array.from(this.currentAction.effort.keys());
	}

	// -----------------------------
	//  Private properties
	// -----------------------------

	private _isCollapsed = true;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Constructor.
	 */
	constructor(
		private readonly dataService: DataService,
		private readonly localizationPipe: LocalizationPipe
	) {}

	/**
	 * Expand or collapse the action container.
	 *
	 * @returns {void}
	 */
	switchCollapse(): void {
		this._isCollapsed = !this._isCollapsed;
	}

	/**
	 * Return filter text.
	 *
	 * @param {FilterType} filterType
	 * @param {filterId} filterId
	 * @returns {string}
	 */
	getText(filterType: FilterType, filterId: string): string {
		let str = '';
		if (filterType === FilterType.effort) {
			str = `${FilterType[filterType]}-${filterId}-${this.currentAction.effort.get(filterId)}`;
		}

		if (filterType === FilterType.impact) {
			str = `${FilterType[filterType]}-${filterId}-${this.currentAction.impact.get(filterId)}`;
		}

		return this.localizationPipe.transform(str);
	}

	trackByImpactFn = (index, item: string) => index;

	// -----------------------------
	//  Private functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}

}
