import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {HorizontalBarComponent} from './horizontal-bar.component';

describe('HorizontalBarComponent', () => {
	let component: HorizontalBarComponent;
	let fixture: ComponentFixture<HorizontalBarComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [HorizontalBarComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HorizontalBarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
