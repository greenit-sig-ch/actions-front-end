import { Component, Input, OnInit } from '@angular/core';
import { IdValueColor } from '../../interfaces/id-value-color';

interface IHBItem {
	key: string;
	color: string;
	width: string;
}

@Component({
	selector: 'app-horizontal-bar',
	templateUrl: './horizontal-bar.component.html',
	styleUrls: ['./horizontal-bar.component.less']
})
export class HorizontalBarComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------

	@Input()
	barHeight = 5;

	@Input()
	map: Map<string, number>;

	@Input()
	store: Map<string, IdValueColor>;

	@Input()
	keysToShow: Array<string>;

	// -----------------------------
	//  Public properties
	// -----------------------------

	barList: Array<IHBItem>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	trackByKeys = (index, item: IHBItem) => item.key;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
		this.barList = this.keysToShow.map(key => {
			return {
				key,
				color: `${this.store.get(key).color}`,
				width: `${this.map.get(key) * 100 / this.store.get(key).maxValue}%`
			};
		});
	}

}
