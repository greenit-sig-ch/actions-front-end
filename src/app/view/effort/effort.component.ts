import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { ChangeContext, Options } from '@angular-slider/ngx-slider';

import { IdValueColor } from '../../interfaces/id-value-color';
import { EffortsType } from '../../vo/filter-set';

@Component({
	selector: 'app-effort',
	templateUrl: './effort.component.html',
	styleUrls: ['./effort.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: EffortComponent,
		multi: true
	}]
})
export class EffortComponent implements OnInit, OnChanges, ControlValueAccessor {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Current effort filter configuration.
	 */
	@Input()
	currentEffort: IdValueColor;

	// -----------------------------
	//  Output properties
	// -----------------------------

	@Output()
	readonly selected: EventEmitter<void> = new EventEmitter<void>();
	
	// -----------------------------
	//  Public properties
	// -----------------------------
	
	lowValue: number = 0;
	maxValue: number = 100;
	
	options: Options;
	
	// -----------------------------
	//  Private properties
	// -----------------------------

	private onTouchedCallback: () => {};
	private onChangeCallback: () => {};

	/**
	 * Slider model.
	 */
	private model: EffortsType;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * On slide end event handler.
	 *
	 * @param event
	 */
	onUserChange(event: ChangeContext): void {
		this.model[this.currentEffort.id] = [ event.value, event.highValue ];
		this.selected.emit();
	}

	// -----------------------------
	//  ControlValueAccessor
	// -----------------------------

	writeValue(value: EffortsType): void {
		if (value && this.currentEffort) {
			this.model = value;
			[ this.lowValue, this.maxValue ] = this.model[this.currentEffort.id];
		}
	}

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}
	
	ngOnChanges(changes: SimpleChanges): void {
		if (changes['currentEffort']) {
			this.options = {
				floor: 0,
				ceil: this.currentEffort.maxValue,
				showTicksValues: false,
				showTicks: false,
				hidePointerLabels: true,
				hideLimitLabels: true,
				tickValueStep: 1,
			};
		}
	}
	
}
