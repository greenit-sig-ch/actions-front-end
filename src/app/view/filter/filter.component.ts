import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
	TrackByFunction
} from '@angular/core';

import { FilterType } from '../../enums/filter-type';
import { IdValueColor } from '../../interfaces/id-value-color';
import { DataService } from '../../service/data.service';
import { LoggerService, MODE } from '../../service/logger.service';
import { FilterSet } from '../../vo/filter-set';
import { Subject, takeUntil } from 'rxjs';
import { Actions } from '../../vo/actions';

interface IFilterItem {
	filter: string;
	valuesByFilter: Array<string>;
}

@Component({
	selector: 'app-filter',
	templateUrl: './filter.component.html',
	styleUrls: ['./filter.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterComponent implements OnInit, OnDestroy {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Get impact filter values.
	 */
	@Input()
	get impactValues(): Array<IdValueColor> {
		if (this.dataService.getActions()
			&& this.dataService.getActions().impact) {
			return this.dataService.getActions().impact;
		}

		return [];
	}

	// -----------------------------
	//  Output properties
	// -----------------------------

	@Output()
	readonly helpClicked: EventEmitter<void> = new EventEmitter();

	@Output()
	readonly toggleMenu: EventEmitter<void> = new EventEmitter();

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Get filter set model.
	 */
	get model(): FilterSet {
		return this.dataService.getFilterSet();
	}

	/**
	 * Get all efforts.
	 */
	get allEfforts(): Array<IdValueColor> {
		if (this.dataService.getActions()) {
			return this.dataService.getActions().effort;
		}

		return [];
	}
	
	// -----------------------------
	//  Public properties
	// -----------------------------
	
	readonly trackByAllFilters: TrackByFunction<IFilterItem> = (index: number, item: IFilterItem) => item.filter;
	
	readonly FilterType = FilterType;
	readonly allFilters: Array<IFilterItem> = [];
	
	// -----------------------------
	//  Private properties
	// -----------------------------
	
	private readonly destroySource = new Subject<void>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of FilterComponent.
	 *
	 * @param dataService
	 * @param logger
	 * @param cdr
	 */
	constructor(
		private readonly dataService: DataService,
		private readonly logger: LoggerService,
		private readonly cdr: ChangeDetectorRef,
	) {}
	
	/**
	 * Change filter value handler.
	 *
	 * @param event
	 * @param filterType
	 */
	onFilterItemChange(event: Event, filterType: string): void {
		this.logger.log(MODE.log, this, 'onFilterItemChange', filterType, this.dataService.getFilterSet());
		this.dataService.performFilterActions();
	}
	
	/**
	 * Change effort filter handler.
	 */
	onEffortChange(): void {
		this.logger.log(MODE.log, this, 'onEffortChange', FilterType.effort, this.dataService.getFilterSet());
		this.dataService.performFilterActions();
	}

	/**
	 * Reset all filters.
	 */
	resetFilter(): void {
		this.model.reset();
		this.dataService.performFilterActions();
	}

	/**
	 * On help button click handler.
	 * Dispatches the "helpClicked" event.
	 */
	showHelp(): void {
		this.logger.log(MODE.info, this, 'showHelp clicked; trying to send event to parent.');
		this.helpClicked.emit();
	}

	/**
	 * Close menu click handler.
	 * Dispatches the "toggleMenu" event.
	 */
	closeMenu(): void {
		this.toggleMenu.emit();
	}
	
	// -----------------------------
	//  Lifecycle functions
	// -----------------------------
	
	ngOnInit(): void {
		this.dataService.actionSubject$
			.pipe(takeUntil(this.destroySource))
			.subscribe((actions: Actions) => {
				this.allFilters.splice(0);

				if (actions) {
					const arr = actions.filterTypes.map((filter: string) => ({
						filter,
						valuesByFilter: this.getFilterValuesByType(filter)
					}));

					this.allFilters.push(...arr);
				}

				this.cdr.markForCheck();
			});
	}
	
	ngOnDestroy(): void {
		this.destroySource.next();
		this.destroySource.complete();
	}
	
	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Get filter values by filter type.
	 *
	 * @param filterType
	 */
	private getFilterValuesByType(filterType: string): Array<string> {
		if (this.dataService.getActions()
			&& this.dataService.getActions().filter
			&& this.dataService.getActions().filter
				.has(filterType)
		) {
			return this.dataService.getActions().filter
				.get(filterType);
		}

		return [];
	}

}
