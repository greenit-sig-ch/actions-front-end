# Content directory

This directory must contain the app's content files:

* **config.yaml**: The app's configuration file
* **Measures definition file**: A YAML file that defines the app's filters and measures
* **Language files**: YAML files that contain the action list's strings; one file per language

Sample files that demonstrate the features of the catalogue of measures app can be found in /doc/sample.

## Important

This directory contains a .gitignore file that makes Git ignore all files
except this README and the .gitignore file itself.

**If you want to track your content files in a Git repository of your own, you
should update or remove the .gitignore file.**

We recommend to use a separate repository for the app's content.
