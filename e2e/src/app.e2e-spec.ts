import {AssessmentActionFrontEndPage} from './app.po';

describe('assessment-action-front-end App', () => {
	let page: AssessmentActionFrontEndPage;

	beforeEach(() => {
		page = new AssessmentActionFrontEndPage();
	});

	it('should display message saying app works', () => {
		page.navigateTo();
		expect<any>(page.getParagraphText()).toEqual('app works!');
	});
});
